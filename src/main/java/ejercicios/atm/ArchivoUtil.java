package ejercicios.atm;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

import ejercicios.atm.exception.ArchivoExcepcion;

/**
 * Clase utilitaria para manejo de archivos. 
 * @author Vicente
 *
 */
public final class ArchivoUtil 
{
	private String nombreArchivo;

	public ArchivoUtil(String nombreArchivo){
		this.nombreArchivo = nombreArchivo;
	}
	/**
	 * Metodo que obtiene los datos desde el archivo y 
	 * lo retorna en formato de lista de string
	 * @return lista de string
	 * @throws ArchivoExcepcion
	 */
	public List<String> obtenerDatosDesdeArchivo() throws ArchivoExcepcion
	{
		List<String> datos = new ArrayList<String>();
		
		// TODO: 1. Crear una variable de tipo BufferedReader
		// TODO: 2. Llamar al metodo leerArchivo() que retorna los datos del archivo dentro de un BufferedReader
		// TODO: 3. Iterar el BufferedReader, en cada iteracion agregar la cadena dentro de la lista datos
		// TODO: 4. Manejar la exception IOException. Relanzar una excepcion del tipo ArchivoExcepcion
		//          con un mensaje apropiado
		// TODO: Retornar lista datos
		// TODO: Agregar bloque finally y cerrar el recurso BufferedReader, manejar la excepcion, pero dejar en blanco el bloque catch
 		return null;
	}
	
	
	
	/**
	 * Metodo utilitario que obtiene el archivo y lo retorna en un BufferedReader
	 * @param nombre Nombre del archivo a leer
	 * @return BufferedReader con el contenido del archivo
	 * @throws ArchivoExcepcion En caso que no exista el archivo.
	 */
	private static BufferedReader leerArchivo(String nombre) throws ArchivoExcepcion
	{
		//TODO: Crear una instancia de la clase File, utilizando el nombre de archivo recibida como argumento
		//TODO: Crear una instancia de la clase FileReader
		//TODO: Crear una instancia de la clase BufferedReader. 
		//TODO: Retornar BufferedReader. 
		//TODO: Manejar exception FileNotFounException y relanzar una exception ArchivoException. 
		return null;
	}
	
	/**
	 * Escribe la lista recibida en el archivo; 
	 * @param datos Lista a escribir en el archivo
	 * @throws ArchivoExcepcion En caso que no exista el archivo. 
	 */
	public void escribirEnArchivo(List<String> datos) throws ArchivoExcepcion
	{
		//TODO: Crear una instancia de la clase File, utilizando el nombre almacenado en la variable de instancia
		//TODO: Crear una instancia de la clase FileWriter
		//TODO: Crear una instancia de la clase PrintWriter.
		//TODO: Iterar la lista recibida como argumento, por cada iteracion llamar 
		//      al metodo println pasando como argumento el valor del string iterado
		//TODO: Llamar al metodo flush
		//TODO: Manejar la exception, relanzar un ArchivoExcepcion
		//TODO: Cerrar el recurso PrintWriter
		
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	
	
}
