package ejercicios.atm;

public class SolicitudSaldo extends Transaccion {	
	@Override
    public void ejecutar() {
    	Double saldo = baseDatos.obtenerSaldoDisponible(getNumeroCuenta());
        pantalla.limpiarPantalla();
        pantalla.mostrarMensaje("El saldo es: " + saldo);
        teclado.hacerPausa();
    }
    
}
