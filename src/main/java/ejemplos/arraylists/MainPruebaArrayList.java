package ejemplos.arraylists;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Clase PruebaArrayList
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class MainPruebaArrayList {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        //En este arrayList almacenaremos objetos del tipo int
        ArrayList<Integer> a = new ArrayList<Integer>();        
        a.add(6);
        a.add(8);        
        a.add(1);
                
        imprimirArrayList(a);
        
        imprimirArrayListConIterador(a);
    } //Fin de main
    
    public static void imprimirArrayList(ArrayList<Integer> a) {
        System.out.printf("\nImpresion del arrayList con for: ");
        
        for (int i = 0; i < a.size(); i++) {
            int val = (int) a.get(i);            
            System.out.printf(" %d ", val);
        }
        
        System.out.printf("\n");
    }
    
    public static void imprimirArrayListConIterador(ArrayList<Integer> a) {
        System.out.printf("\nImpresion del arrayList con Iterador: ");
        
        Iterator<Integer> it = a.iterator();
        while (it.hasNext()) {
            int val = (int) it.next();
            System.out.printf(" %d ", val);
        }
        
        System.out.printf("\n");
    }

} //Fin de clase PruebaArrayList
