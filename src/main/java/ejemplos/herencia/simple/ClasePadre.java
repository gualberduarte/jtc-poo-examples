package ejemplos.herencia.simple;

/**
 * Clase ClasePadre
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class ClasePadre {
    
    public int nroPub; //sera visible desde cualquier lado esta variable porque es publica
    
    private int nroPriv; //solo es visible en esta clase
    
    int nroFriend; //esta variable solo es visible en clases del mismo paquete
    
    protected int nroProt; //solo es visible en esta clase y en sus clases derivadas
    
    public ClasePadre() {
        System.out.println("Constructor vacio en ClasePadre");
    }

    public ClasePadre(String nombreClase) {
        System.out.println("Constructor en clasePadre de clase " + nombreClase);
    }
    
    public void metodoPubEnPadre() {
        System.out.println("Metodo publico en padre");
    }
    
    private void metodoPrivEnPadre() {
        System.out.println("Metodo privado en padre");
    }
    
    void metodoAmistosoEnPadre() {
        System.out.println("Metodo amistoso en padre");
    }
    
    protected void metodoProtegidoEnPadre() {
        System.out.println("Metodo protegido en padre");
    }
    
    public void test() {
        System.out.println("Este metodo test hace algo aqui y luego invoca a cosas privadas de la clase...");
        metodoPrivEnPadre();
    }
    
    //Este metodo no puede ser redefinido en las clases hijas
    public final int calcularSumaNoRedefinible(int a, int b) {
        System.out.println("Calcula la suma en padre...");
        return a + b;
    }
    
    public int calcularSumaRedefinible(int a, int b) {
        System.out.println("Calcula la suma en padre...");
        return a + b;
    }
} 