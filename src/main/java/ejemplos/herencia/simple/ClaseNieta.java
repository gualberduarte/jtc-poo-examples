package ejemplos.herencia.simple;

/**
 * Clase ClaseNieta
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class ClaseNieta extends ClaseHija {
    
    public void metodoPubEnNieta() {
        System.out.println("Metodo publico en clase nieta");
    }
} 