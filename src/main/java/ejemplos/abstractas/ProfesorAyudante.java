package ejemplos.abstractas;

/**
 * Clase ProfesorEscalafonado
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class ProfesorAyudante extends Profesor {

    @Override
    public void imprimir() {
        System.out.println("Impresion de profesor ayudante");
    }



} //Fin de clase ProfesorEscalafonado
